import logging
import multiprocessing
import os

import pandas

from const import RELEASES, OCDS_DATASET
from elastic_helper import insert_into_elastic_search
from helper import get_directory, convert_to_json, get_connection

manager = multiprocessing.Manager()
all_ocids = manager.list()

def generate_csv(query_path, year=None, folder=OCDS_DATASET, name=None, params=None,
                 prefijo=None, clave=None):
    database_connection = get_connection()
    logging.info('EXTRACT: data set de %s buscando fechas % s' % (name, params))
    query = open(query_path, 'r')
    directory = get_directory(folder, name, year)
    results = pandas.read_sql_query(query.read().format(*params), database_connection) if params else \
        pandas.read_sql_query(query.read(), database_connection)
    if prefijo:
        results.columns = [prefijo + str(col.replace(clave, '')) if clave in col else str(col) for col in
                           results.columns]
    results.to_csv("{}/{}.csv".format(directory, name), index=False, encoding='utf-8', float_format="%d")
    database_connection.close()
    query.close()
    return directory


def load(json_file, dataset_set):
    all_ocids.extend(insert_into_elastic_search(json_file, dataset_set))


def transform_ocds(year):
    json_file_name = '{}-{}.json'.format(RELEASES, year)
    directorio = get_directory(folder=OCDS_DATASET, name=None, anio=year)
    convert_to_json(directorio, json_file_name, RELEASES)


def load_releases_ocds(path):
    json_file_name = '{}-{}.json'.format(RELEASES, path)
    directorio = get_directory(folder=OCDS_DATASET, name=None, anio=path)
    json_path = os.path.join(directorio, json_file_name)
    dataset = {'name': RELEASES, 'type': 'release', 'id': 'id', 'table': RELEASES}
    load(json_path, dataset)


def extract_ocds_for_dataset(dataset, params, year, args):
    # si se pasa el parametro de datasets entonces solo se ejecutan los queries de los datasets indicados
    if (args.datasets and dataset['name'] in args.datasets) or not args.datasets:
        generate_csv(dataset['query_path'], year=year, folder=OCDS_DATASET, name=dataset['name'],
                     params=params, prefijo=dataset['prefijo'] if 'prefijo' in dataset else None,
                     clave=dataset['clave'] if 'clave' in dataset else None)


def extract_ocds(conjuntos_datos, params, year, args):
    datos_ocds = conjuntos_datos[OCDS_DATASET]
    for dataset in datos_ocds:
        extract_ocds_for_dataset(dataset, params, year, args)