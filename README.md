[English](https://gitlab.com/dncp-opendata/opendata-etl/blob/master/README_en.md)

# [Kavure'i](https://gn.wikipedia.org/wiki/Kavure%27i): ETL para un portal de datos Abiertos

![Image of Kavurei](https://upload.wikimedia.org/wikipedia/commons/5/59/Cactus_Ferruginous_Pygmy-owl.jpg)


Este proyecto consiste en un ETL que extrae datos de una base de datos postgresql, 
mediante consultas SQL en formato flattenizado, según [https://flatten-tool.readthedocs.io/en/latest/examples/](https://flatten-tool.readthedocs.io/en/latest/examples/), 
las guarda como CSV usando [pandas](https://pandas.pydata.org/), las transforma a JSON usando un [flatten-tool.](https://flatten-tool.readthedocs.io/) ,
los inserta como JSON individuales en una base de datos [Elastic Search](https://www.elastic.co/es/), genera los records dado todos los releases insertados
al ElasticSearch y finalmente genera un csv final con los compiled releases almacenados en el ElasticSearch para disponibilizarlo como archivo masivo de los datos.

## Formato de queries

Un ejemplo de cómo deben estar definidos los queries puede ser encontrado en la carpeta queries.
Consideraciones para los queries OCDS:
- Deben tener el formato flattenizado del estándar, definido en [https://flatten-tool.readthedocs.io/en/latest/examples/](https://flatten-tool.readthedocs.io/en/latest/examples/)
- Para que  un query se ejecute, debe listarse en el archivo datasets_conf.yml. Si es un query de un release OCDS entonces debe ir bajo
el apartado de *ocds* si es un dataset diferente debe ir en el apartado de *no_ocds*
- Los parámetros de filtros a pasar al query deben especificarse con el formato {0} donde el 0 es el orden de los parámetros a pasar.
El script está preparado apra recibir 2 parámetros para OCDS, una fecha de inicio y una de fin para buscar los datos.
- Cada query generará un archivo CSV, que en su conjunto luego serán transformados a 1 archivo JSON de releases OCDS
- Cada query y por tanto cada archivo CSV resultante está compuesto por objetos o arrays OCDS. Por ejemplo, existe un query para obtener todos los tenders, 
que será finalmente el csv tender.csv y luego para cada array dentro de tender, por ejemplo documents, se deberá agregar un csv nuevo, indicando en este 
nuevo query de documentos el id del tender al que el documento corresponde (ver en el ejemplo el campo tender/id dentro del archivo llamado-documentos.sql)
Esto hará que la herramienta de conversión a JSON pueda unir el objeto tender con el array de todos sus documentos y lo mismo para cada uno de los arrays
que tenga el objeto tender y los demás objetos del estándar OCDS.
- Para poder unir todos los datos entre sí, por ejemplo, tender con sus tender/documents los queries especificados en ambos archivos deben además
compartir un mismo **id** del release, con este id la herramienta de flatten es capaz de juntar la información en un mismo objeto, en este caso tender.
- Se recomienda que los queries, y por lo tanto releases, estén separados por un rango de tiempo, como se muestra en el querie de ejemplo, dependiendo
del volumen de datos este rango puede ser de un mes, un año, etc. Esto está sujeto a la performance que tiene el flatten tool para realizar la conversión
de csv a json sin ocupar toda la memoria de la computadora en la que se está corriendo el script.

## Formato de archivo datasets_conf

Este archivo lista todos los queries a ejecutar y los agrupa según sean releases ocds u otro tipo de conjunto de dato.
Si el query a agregar es parte de un release ocds entonces debe ir bajo la lista *ocds*, si no bajo la lista *no-ocds*.
Para los queries ocds hay que especificar los campos:
- name: el nombre del objeto a a generar, este nombre es utilizado luego como nombre del csv a generar
- query_path: el path al query a ejecutar
Opcionalmente, dado que en postgresql los nombres de las columnas solo pueden tener hasta 63 caracteres y puede que en formato
flatenizado algunas columnas tengas más que esta cantidad, se puede especificar en los queries el nombre de la columna con un prefijo,
que luego será reemplazado por su nombre correcto al generar el CSV, por ejemplo:
    - clave: linea
    - prefijo: planning/budget/budgetBreakdown/0
   
Para los queries *no-ocds*, se debe especificar los siguientes campos, por ejemplo, si se tiene el conjunto de datos de proveedores:
  - nombre: nombre del índice en elasticsearch, ejemplo: proveedores
  - table: nombre del csv e índice a crear en el elasticsearch, ejemplo: proveedores
  - id: nombre del campo a utilizar como id, debe ser único en el dataset, ejemplo id
  - type: tipo de objeto en el elasticsearch a crear, ejemplo proveedor
  - datasets: listado de queries pertenecientes a este datasets, pueden ser varios si el objeto tiene arrays como propiedad, y para cada dataset:
      - id: id
      - name: proveedores
      - query_path: queries/proveedores/proveedores.sql

## Variables de entorno y datos a configurar

Para poder ejecutar los scripts 3 variables de entorno deben configurarse:

 - DATABASE_URL: la url a la base de datos de la cual se extraerán los datos
 - ELASTIC_URL: la url a la base de datos elastic search en la cual se cargarán los datos
 - DATA_PATH: el path en el cual se guardarán los datos transformados
 
Además, en el archivo const.py en la variable PACKAGE_DATA se deben configurar los meta datos de la publicación OCDS, 
tales como:
- El publicador
- La URL a la política de publicación
- La URL a la licencia
- La versión del estándar utilizada
- La lista de extensiones utilizadas

Además la variable INITIAL_YEAR se debe configurar para determinar desde qué año se deben buscar los datos para su conversión.

## Ejecución

### Con virtual env
- Crear un entorno virtual ejecutando: ```virtualenv -p python3 .ve```
- Activar el entorno virtual con ```source .ve/bin/activate```
- Instalar las dependencias del proyecto con ```pip install -r requirements.txt```
- Setear las variables de entorno DATABASE_URL, DATA_PATH y ELASTIC_URL por ejemplo:
    ```export DATABASE_URL=test``` 
    ```export ELASTIC_URL=http://172.17.0.1:9200```
    ```export DATA_PATH=/home/user/data```
- Existen dos posibles scripts a ejecutar:

1) convert_historical: Este script está preparado para hacer una búsqueda masiva por mes desde el año seteado en INITIAL_YEAR hasta el año 
actual. Para ejecutarlo hay que correr ```python covert_historical.py```. Este comando se debería ejecutar solo 1 vez para tener los archivos
masivos de todos los datos existentes. De igual forma, posee las siguientes opciones:
- --delete-indexes: Borra los indices OCDS de elasticsearch antes de empezar
- --only-ocds: 'Solo realiza el ETL para los queries especificados bajo ocds
- --only-no-ocds: Solo realiza el ETL para los conjuntos de datos no ocds
- --only-transform: Solo se realiza el paso de transformacion (conversión a JSON de CSVs ya existentes)
- --only-extract: 'Solo se realiza el paso de extracción (ejecución de queries)
- --only-load: Solo se realiza el paso de carga (carga de JSON existentes a ElasticSearch)
- -d: Indica la lista de datasets a ejecutar
- --only-final-csv: Solo genera los csv finales (esto es el csv con los releses por año)

2) convert: este script está preparado para ejecutar los queries teniendo en cuenta la última fecha de ejecución del script, es decir el rango
de fechas a buscar va desde la última fecha de ejecución hasta la fecha actual. Este script es el que debe ser configurado, por ejemplo, en un CRON
para actualizar los datos según una frecuencia determinada, por ejemplo, cada 1 día. Se ejecuta con ```python covert.py``` y tiene las siguientes opciones:
- --only-ocds: 'Solo realiza el ETL para los queries especificados bajo ocds
- --only-no-ocds: Solo realiza el ETL para los conjuntos de datos no ocds
- -d: Indica la lista de datasets a ejecutar
- --only-final-csv: Solo genera los csv finales (esto es el csv con los releses por año)

El proceso de generación de archivos finales de 1 json/csv por año está separado porque puede que el usuario quiera, por ejemplo, actualizar los archivos
masivos 1 vez por día pero actualizar el ElasticSearch con más regularidad, por ejemplo cada 1 hora. 
